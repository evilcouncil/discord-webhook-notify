"""Posts a message to a discord webhook."""
import dotenv
import os
import asyncio
import aiohttp
import discord


async def main():
    """Load configs"""
    dotenv.load_dotenv()
    msg = os.environ["MESSAGE"]
    webhook_url = os.environ["WEBHOOK_URL"]

    # send message
    async with aiohttp.ClientSession() as session:
        hook = discord.Webhook.from_url(webhook_url, session=session)
        await hook.send(msg)


if __name__ == "__main__":
    asyncio.run(main())
