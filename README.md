discord\_webhook\_notify
---

Container that posts a message to a discord webhook

## Environment Variables

```
MESSAGE - message text to post to webhook

WEBHOOK_URL - url of the webhook

```
