
FROM	ubuntu:24.04
RUN	apt update \
	&& apt install -y python3-discord python3-dotenv
COPY	src	/app
WORKDIR	/app
CMD	["/usr/bin/python3","/app/main.py"]
